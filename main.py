import json
from tkinter import *
from tkinter import messagebox
import pyperclip
import random

# ---------------------------- PASSWORD GENERATOR ------------------------------- #
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
           'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
           'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']


def generate_password():
    pass_i.delete(0, END)

    password_letters = [random.choice(letters) for _ in range(random.randint(8, 10))]
    password_symbols = [random.choice(numbers) for _ in range(random.randint(2, 4))]
    password_numbers = [random.choice(symbols) for _ in range(random.randint(2, 4))]

    password_list = password_letters + password_numbers + password_symbols
    random.shuffle(password_list)

    password = ''.join([x for x in password_list])
    pass_i.insert(0, password)
    pyperclip.copy(password)


# ---------------------------- SAVE PASSWORD ------------------------------- #

def clear(w, e):
    w.delete(0, END)
    e.delete(0, END)


def info(bl: bool):
    if bl:
        complete.config(text='Saved!', fg="GREEN")
    else:
        complete.config(text="Please complete the fields", fg="RED")
    complete.grid(column=1, row=5, columnspan=2)


def save():
    web = web_i.get()
    passwd = pass_i.get()
    email = eu_i.get()
    new_data = {web: {
        "email": email,
        "password": passwd
    }
    }
    if not web or not passwd:
        info(False)
    else:
        info(True)
        try:
            with open("data.json", 'r') as data_file:
                data = json.load(data_file)  # przypisz dane w pliku do :data:
        except FileNotFoundError:
            with open("data.json", 'w') as data_file:
                json.dump(new_data, data_file, indent=4)
        else:
            data.update(new_data)  # update :data:
            with open("data.json", 'w') as data_file:
                json.dump(data, data_file, indent=4)  # zapisz :data: do :data_file:
        finally:
            clear(web_i, pass_i)


# ---------------------------- FIND PASSWORD ------------------------------- #
def find_password():
    web = web_i.get()
    try:
        with open("data.json", "r") as data_file:
            data = json.load(data_file)

    except FileNotFoundError:
        messagebox.showinfo("No file", "No Data File Found.")
    else:
        if web in data.keys():
            messagebox.showinfo(title='Saved passwd:',
                                message=f"Website: {data[web]['email']}\n Password: {data[web]['password']}")
        else:
            messagebox.showinfo("Error", f"No details for {web} exist.")


# ---------------------------- UI SETUP ------------------------------- #

window = Tk()
window.title('Passwd creator')
window.config(padx=55, pady=55)

# Photo:
canvas = Canvas(width=200, height=200, highlightthickness=0)
photo = PhotoImage(file='logo.png')
canvas.create_image(100, 100, image=photo)
canvas.grid(column=1, row=0)

# Website:
Label(text="Website:").grid(column=0, row=1)
web_i = Entry(width=20)
web_i.grid(column=1, row=1, columnspan=1)
web_i.focus()

# Email/Username:
Label(text="Email/Username:").grid(column=0, row=2)
eu_i = Entry(width=35)
eu_i.grid(column=1, row=2, columnspan=2)
eu_i.insert(0, 'orlowskir44@gmail.com')

# Password:
Label(text="Password:").grid(column=0, row=3)
pass_i = Entry(width=20)
pass_i.grid(column=1, row=3)

# Buttons:
Button(text='Generate Password', width=11, command=generate_password).grid(column=2, row=3, sticky="EW")
add_b = Button(text='Add', width=34, command=save)
add_b.grid(column=1, row=4, columnspan=2)

search_b = Button(text='Search', command=find_password)
search_b.grid(column=2, row=1)

complete = Label()

window.mainloop()
